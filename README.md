# Octopus Developer Tools

Octopus developer tools is a collection of files to aid in the development of the Octopus code.

## Contents

- findent
- fortls
- ctags
- Visual Studio Code IDE
- CLion IDE


In all code snippets, `$OCTOPUS_ROOT` refers to the Octopus code root directory.

## fident

fident is a fortran-specific code formatter, which automatically indents, relabels and converts Fortran sources to 
a standardised format. To implement Octopus's specific formatting style, we utilise a fork of the project, called, 
findent-octopus, which is available from Octopus's [Gitlab](https://gitlab.com/octopus-code/findent-octopus).
Documentation regarding installation and usage is specified in the repository.


## Fortran Language Server

The Fortran language server is an implementation of the Language Server Protocol (LSP) for Fortran using Python (3.7+),
providing autocomplete and other IDE-like functionality. At the time of writing, there are two competing versions of the language server: The original 
[fortran language server](https://github.com/hansec/fortran-language-server) and the [fortls](https://github.com/fortran-lang/fortls).
We recommend the latter, due to its increased functionality and integration with VS Code's 
[Modern Fortran plugin](https://github.com/fortran-lang/vscode-fortran-support).

The `fortls` can be installed straightforwardly with `pip`:

```bash
pip install fortls 
```

To ensure Visual Studio Code is able to parse the Octopus project, it's essential to add the `.fortls` file provided here, 
to the root of the project:

```bash
cp fortls $OCTOPUS_ROOT/fortls
```


## CTags

ctags generates an index (or tag) file of language objects found in source files for programming languages. This index 
makes it easy for text editors and other tools to locate the indexed items.

Ctags has forked into two projects: [Universal Ctags](https://github.com/universal-ctags/ctags) (abbreviated as u-ctags) 
and [Exuberant Ctags](https://ctags.sourceforge.net) (e-ctags). While Universal Ctags is the actively-maintained project, 
Exuberant-ctags is more widely available.


### Installing Exuberant CTags on Linux

* For Debian-based systems (Ubuntu, Mint, etc.):

```bash
sudo apt-get install exuberant-ctags
```

* For Red Hat-based systems (Red Hat, Fedora, CentOS):

```bash
sudo yum install ctags
```

### Installing Exuberant CTags on Mac

```bash
brew install ctags
```

### Installing Universal CTags

Universal ctags availability on Linux can be checked [here](https://pkgs.org/download/ctags), whilst availability on 
Mac is documented [here](https://github.com/universal-ctags/homebrew-universal-ctags).


### Configuring CTags

```bash
# Move the ctags file into $HOME
mv ctags ~/.ctags
# Generate .tags file for octopus (.ctags in $HOME will be found)
cd $OCTOPUS_ROOT/src
ctags
# If `tags` has been generated, move it to .tags (observed on Mac)
```

Once installed and configured, routines can be jumped into using `ctrl-t` on Linux, or `cmd-t` on Mac.

CTags support for VS Code is included in the `extensions.json`. 


## Visual Studio Code 

Visual Studio Code, also commonly referred to as VS Code, is a source-code editor made by Microsoft. 

The `.vscode` folder can be used to define and share settings, task configuration and debug configuration between developers.
To copy the folder to the project root:

```bash
cp vscode $OCTOPUS_ROOT/.vscode
```

### .vscode Files

* `extensions.json`: Define recommended VS Code extensions


## CLion 

CLion is a smart editor with refactorings, code analysis, and unit test support. Specifically written for C/C++, it 
also supports fortran (along with its sister IDE, Pycharm). CLion and Pycharm are 
[free for academics](https://www.jetbrains.com/clion/buy/#discounts).
